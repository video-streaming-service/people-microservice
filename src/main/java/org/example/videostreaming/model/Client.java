package org.example.videostreaming.model;


import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.List;


@Setter
@Getter
@Entity
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
@Table(name="client")

public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int user_id;

    @Column(nullable = false, unique = true)
    @Size(min = 10)
    @NotBlank
    @NonNull
    private String nickname;

    @Column(nullable = false, unique = true)
    @Size(min = 10)
    @NotBlank
    @NonNull
    private String login;

    @Column(nullable = false, unique = true)
    @Email
    @NotBlank
    @NonNull
    private String mail;

    @Column(nullable = false)
    @NotBlank
    @NonNull
    private String password;


    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "friendship",
               joinColumns = @JoinColumn(name = "user_id_1"),
               inverseJoinColumns = @JoinColumn(name="user_id_2"),
               uniqueConstraints = @UniqueConstraint(name="friendship_users_key", columnNames = {"user_id_1", "user_id_2"}))
    private List<Client> friends;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "subscribe",
            joinColumns = @JoinColumn(name = "user_id_1"),
            inverseJoinColumns = @JoinColumn(name="user_id_2"),
            uniqueConstraints = @UniqueConstraint(name="uniqueSubscribe", columnNames = {"user_id_1", "user_id_2"}))
    private List<Client> subscribers;


}
