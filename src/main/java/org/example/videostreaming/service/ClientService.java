package org.example.videostreaming.service;

import jakarta.validation.Valid;
import org.example.videostreaming.model.Client;

import java.util.List;

public interface ClientService {

    Client saveClient(@Valid Client client);

    List<Client> getClients();

    Client getClient(int id);

    void deleteClient(int id);

    Client updateClient(int id, @Valid Client client);
}
