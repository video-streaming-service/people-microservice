package org.example.videostreaming.service;

import jakarta.validation.Valid;
import org.example.videostreaming.model.Client;
import org.example.videostreaming.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.util.List;
@Service
@Validated
@ComponentScan("org.example.videostreaming.repository")
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public Client saveClient(@Valid Client client) {
        return clientRepository.save(client);
    }

    @Override
    public List<Client> getClients() {
        return (List<Client>) clientRepository.findAll();
    }

    @Override
    public Client getClient(int id) {

        return clientRepository.findById(id).isPresent()? clientRepository.findById(id).get():null;
    }

    @Override
    public void deleteClient(int id) {
        clientRepository.deleteById(id);
    }

    @Override
    public Client updateClient(int id, @Valid Client client) {
        Client clientToUpdate = getClient(id);

        clientToUpdate.setNickname(client.getNickname());
        clientToUpdate.setLogin(client.getLogin());
        clientToUpdate.setPassword(client.getPassword());

        return clientRepository.save(clientToUpdate);
    }
}
